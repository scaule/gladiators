<?php


class Arena{

    public static function fight(Gladiator $gladiator1, Gladiator $gladiator2){
        echo "Les fighters sont : ".$gladiator1->getName()." et ".$gladiator2->getName(). "\n";

        while ($gladiator1->isAlive() && $gladiator2->isAlive()){
            $gladiator1->isAttackedBy($gladiator2);
            $gladiator2->isAttackedBy($gladiator1);
        }

        return $gladiator1->isAlive() ? $gladiator1 : $gladiator2;
    }
}
