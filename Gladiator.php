<?php

class Gladiator{

    private $name;
    private $pv;
    private $attack;

    public function __construct($name)
    {
        $this->name = $name;
        $this->pv = rand(0, 100);
        $this->attack = rand(0,30);
    }

    public function isAlive()
    {
        if($this->pv > 0){
            return true;
        }

        return false;
    }

    public function isAttackedBy(Gladiator $gladiator)
    {
        $this->pv -= $gladiator->getAttack();
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public function getAttack(){
        return $this->attack;
    }
}
