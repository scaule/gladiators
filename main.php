<?php

//include files
include_once ('Arena.php');
include_once ('Gladiator.php');
include_once ('CliUtil.php');
include_once ('GladiatorManager.php');



//create gladiators
$gladiators = [];
$count = CliUtil::getFromCli("Combien voulez-vous de gladiateurs ?");

for($i=0; $i < $count; ++$i){
    $gladiators[] = GladiatorManager::createGladiatorFromCli();
}

//Choose 2 gladiators by rand 
shuffle($gladiators);
$winner = Arena::fight($gladiators[0], $gladiators[1]);

echo "Le gagnant est ".$winner->getName(). "\n";
