<?php

class GladiatorManager{
    public static function createGladiatorFromCli()
    {
        $name = CliUtil::getFromCli("Quel est le nom du gladiateur ?");

        return new Gladiator($name);
    }
}